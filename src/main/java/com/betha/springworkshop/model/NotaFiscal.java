package com.betha.springworkshop.model;

public class NotaFiscal {

    private Integer id;

    private String nome;

    private String CNPJ;

    private TipoNota tipo;

    public Integer getId() {
        id:
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCNPJ() {
        return CNPJ;
    }

    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }
}
