package com.betha.springworkshop.controllers;

import com.betha.springworkshop.model.NotaFiscal;
import com.betha.springworkshop.model.TipoNota;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping(value = "api/nota-fiscal")
public class NotaFiscalController {

    private ArrayList<NotaFiscal> notaFiscals = new ArrayList();

    @PostMapping
    public ResponseEntity<NotaFiscal> save(NotaFiscal nf){
        notaFiscals.add(nf);
        System.out.println(nf);
        return ResponseEntity.ok(nf);
    }

    @GetMapping
    public ResponseEntity<TipoNota> tipo (){
        return ResponseEntity.ok(TipoNota.ACEITA);
    }

}
