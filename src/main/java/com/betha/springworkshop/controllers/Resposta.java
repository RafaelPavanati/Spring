package com.betha.springworkshop.controllers;


public class Resposta {
    private Double imc;
    private String mensagem;

    public Resposta(Double peso, Double altura) {
        this.imc = peso / (altura * altura);
        this.mensagem = String.format(" seu imc é: %.2f" , this.imc);
    }

    public Double getImc() {
        return imc;
    }

    public String getMensagem() {
        return mensagem;
    }
}
