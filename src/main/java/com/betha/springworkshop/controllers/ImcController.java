package com.betha.springworkshop.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping(value = "api/imc")
public class ImcController {



    @ResponseBody
    @GetMapping()
    public Resposta calcula(Double peso, Double altura) {
        return new Resposta(peso, altura);
    }
}
